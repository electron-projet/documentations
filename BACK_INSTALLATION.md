# Installation BACK

### (WARNING) You need to install golang on your pc ([https://go.dev/](https://go.dev/))
### Before running the server, you need to setup the database (mysql)

```
mysql -u root
CREATE DATABASE electronproject;
CREATE USER 'golang'@'localhost' IDENTIFIED BY 'golang';
GRANT ALL PRIVILEGES ON * . * TO 'golang'@'localhost';
FLUSH PRIVILEGES;
```

Server is running on http://127.0.0.1:8900<br>
8900 is the default port, you can change it inside .env file

```
git clone git@gitlab.com:electron-projet/api.git
```
```
go get 
go run main.go
```