# Installation FRONT

```
git clone git@gitlab.com:electron-projet/front.git
```
```
npm i
```

Make sure that in the .gitmodules (at the root) it is either : 
```
[submodule "src/base-front"]
    path = src/base-front
    url = git@gitlab.com:electron-projet/base.git
```

Then make a 

```
git submodule sync 
git submodule update --init

```
### Before running the server, you need to setup the .env file
```
VUE_APP_PORT=PORT OF YOUR FRONT
VUE_APP_API_HOST=127.0.0.1
VUE_APP_API_PORT=PORT OF YOUR BACK API
```
Default configuration: 
```
VUE_APP_PROTOCOL=https
VUE_APP_HOST=127.0.0.1
VUE_APP_PORT=8080

VUE_APP_API_PROTOCOL=http
VUE_APP_API_HOST=127.0.0.1
VUE_APP_API_PORT=8900
VUE_APP_API_ROOT=/api
```
Then run the serve
```
npm run electron:serve
```



